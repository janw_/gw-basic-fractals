# GW-Basic Fractals
This repository contains GW-Basic (.bas) files for scripts drawing some very nice fractal images.

### Author
All of these scripts were taken from **Hans Lauwerier**'s book _Fracatals, meetkundige figuren in eindeloze herhaling_, published in 1987 by Aramith Uitgevers Amsterdam (ISBN: 90-6834-031-X).

### Notes
- I used an OCR scanner which left little mistakes. It could be some are still there.
- These scripts were originally implemented by Hans Lauwerier on an Olivetti M24 with a 640x400 pixel screen, which was very high-tech at the time. Unfortunatly, I've found that `SCREEN 3` (the resolution) often isn't available for modern emulators, which can sometimes lead to problems displaying the fractals. Luckily for a lot of these fractals other resolutions can be used as well, like `SCREEN 9` (640x350).

### Examples
![Example image](screenshots/BOOM2.png "Example image of BOOM2.BAS fractal")

![Example image](screenshots/BOOMH2.png "Example image of BOOMH2.BAS fractal")

![Example image](screenshots/MANDEL.png "Example image of MANDEL.BAS fractal")

![Example image](screenshots/STOF.png "Example image of STOF.BAS fractal")
